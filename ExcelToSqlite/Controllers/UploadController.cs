﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Excel;
using ExcelToSqlite.Sources;
using ExcelToSqlite.Sources.Data_Providers;
using ExcelToSqlite.Sources.Excel;


namespace ExcelToSqlite.Controllers
{
    public class UploadController : Controller
    {
        [HttpPost()]
        public ActionResult Excel(HttpPostedFileBase File,  DataSetProviderOptions FileOptions)
        {
            var FileName = SaveFile(File);
            SessionSource.SourceFileOriginalName = File.FileName;

            if (Path.GetExtension(File.FileName) == ".xls" || Path.GetExtension(File.FileName) == ".xlsx")
            {
                SessionSource.SourceFileType = Sources.SourceType.Excel;
                SessionSource.DataSource = new ExcelDataProvider(FileOptions, FileName);
            }
            else 
            {
                SessionSource.SourceFileType = Sources.SourceType.CSV;
                SessionSource.DataSource = new CSVDataProvider(FileOptions, FileName);
            }

                       
            return RedirectToAction("Options", "Home");
        }

        private string SaveFile(HttpPostedFileBase File)
        {
            var BasePath = Server.MapPath("~/temp/files/");
            var FileName = String.Format("{0:}_{1}", Guid.NewGuid().ToString(), File.FileName);
            File.SaveAs(BasePath + FileName);
            return BasePath + FileName;
        }
    }
}