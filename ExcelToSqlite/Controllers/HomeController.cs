﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExcelToSqlite.Exports.Sqlite;
using ExcelToSqlite.Models;
using ExcelToSqlite.Sources;

namespace ExcelToSqlite.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Options()
        {
            if (SessionSource.DataSource == null)
            {
                return RedirectToAction("Index");
            }

            var Data = new ExportOptions();
            Data.PopulateFrom(SessionSource.ParseDataSet());
            
            return View(Data);
        }

        [HttpPost]
        public ActionResult Export(ExportOptions Data)
        {            
            var SqlFile = new SQLiteFile(Guid.NewGuid().ToString() + ".sqlite");
            SqlFile.InsertData(SessionSource.ParseDataSet(), Data);

            if (String.IsNullOrWhiteSpace(Data.ExportFileName)) {
                Data.ExportFileName = Guid.NewGuid().ToString() + ".sqlite";
            }

            return File(SqlFile.GetFileStream(), "application/octet-stream", Data.ExportFileName);
        }
    }
}