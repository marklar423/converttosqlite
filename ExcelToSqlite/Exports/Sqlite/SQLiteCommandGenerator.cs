﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Web;
using ExcelToSqlite.Models;

namespace ExcelToSqlite.Exports.Sqlite
{
    public class SQLiteCommandGenerator
    {
        public static IEnumerable<string> GenerateCreateTable(DataSet DS, ExportOptions Options)
        {
            var Commands = new List<string>();

            foreach (DataTable Table in DS.Tables)
            {
                var TableCommand = String.Format("CREATE TABLE [{0}]", Table.TableName);

                var TableOptions = Options.Tables.FirstOrDefault(x => x.NewName == Table.TableName);
                TableCommand += " (" + String.Join(", ", GenerateCreateColumns(Table, TableOptions)) + ");";
                Commands.Add(TableCommand);
            }

            return Commands;
        }

        public static IEnumerable<string> GenerateCreateColumns(DataTable Table, ExportOptionsTable TableOptions)
        {
            var Commands = new List<string>();

            foreach (DataColumn Column in Table.Columns)
            {
                var ColumnOptions = TableOptions.Columns.FirstOrDefault(x => x.NewName == Column.ColumnName);
                Commands.Add(String.Format("[{0}] {1}", Column.ColumnName, ColumnOptions.ColumnType.ToString()));
            }

            return Commands;
        }

        public static IEnumerable<Tuple<string, IEnumerable<SQLiteParameter>>> GenerateInserts(DataSet DS)
        {
            var Commands = new List<Tuple<string, IEnumerable<SQLiteParameter>>>();

            foreach (DataTable Table in DS.Tables)
            {
                var ColumnNames = (from DataColumn C in Table.Columns select C.ColumnName).ToList();
                foreach (DataRow Row in Table.Rows)
                {
                    string Command = String.Format("INSERT INTO [{0}] ([{1}]) VALUES (@{2})", 
                        Table.TableName, 
                        String.Join("], [", ColumnNames), 
                        String.Join(", @", ColumnNames.Select(x => x.Replace(" ", "_"))));

                    IEnumerable<SQLiteParameter> Params = (from C in ColumnNames
                                  select new SQLiteParameter("@" + C.Replace(" ", "_"), Row[C])).ToList();

                    Commands.Add(Tuple.Create(Command, Params));
                }
            }

            return Commands;
        }
    }
}