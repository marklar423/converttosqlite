﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExcelToSqlite.Exports.Sqlite
{
    public enum SQLiteColumnType
    {
        Text = 0,
        Integer = 1,
        Blob = 2,
        Float = 3,
        Datetime,
        Decimal
    }
}