﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Web;

namespace ExcelToSqlite.Exports.Sqlite
{
    public class SQLiteFile
    {
        public static string BaseFilePath
        {
            get
            {
                return HttpContext.Current.Server.MapPath("~/temp/files/sqlite/");
            }
        }

        private string FileName;

        public SQLiteFile(string FileName)
        {
            this.FileName = FileName;
        }

        private SQLiteConnection CreateConnection()
        {
            return new SQLiteConnection("Data Source=" + BaseFilePath + FileName);
        }

        public void InsertData(DataSet DS, Models.ExportOptions Options)
        {
            var NewDS = TransformDataSet(DS, Options);
            var CreateTableCommands = SQLiteCommandGenerator.GenerateCreateTable(NewDS, Options);

            using (var Conn = CreateConnection())
            {
                Conn.Open();

                using (var Transaction = Conn.BeginTransaction())
                {
                    //create tables
                    foreach (var TableCommand in CreateTableCommands)
                    {
                        using (var Cmd = new SQLiteCommand(TableCommand, Conn)) { Cmd.ExecuteNonQuery(); }
                    }

                    Transaction.Commit();
                }


                using (var Transaction = Conn.BeginTransaction())
                {
                    //insert data
                    var DataCommands = SQLiteCommandGenerator.GenerateInserts(NewDS);

                    foreach (var DataCommand in DataCommands)
                    {
                        using (var Cmd = new SQLiteCommand(DataCommand.Item1, Conn)) {
                            foreach (var Param in DataCommand.Item2)
                            {
                                Cmd.Parameters.Add(Param);
                            }

                            Cmd.ExecuteNonQuery(); 
                        }
                    }
                    
                    Transaction.Commit();
                }

                Conn.Close();                
            }

            GC.Collect();
        }

        private DataSet TransformDataSet(DataSet DS, Models.ExportOptions Options)
        {
            var NewDS = new DataSet();

            foreach (DataTable Table in DS.Tables)
            {
                var TableOptions = Options.Tables.Where(x => x.Name == Table.TableName).FirstOrDefault();

                if (TableOptions.ShouldIncludeInExport)
                {
                    var NewTable = NewDS.Tables.Add(TableOptions.NewName);

                    var ColumnNewOldMap = new Dictionary<string, string>();

                    foreach (DataColumn Col in Table.Columns)
                    {
                        var NewName = TableOptions.Columns.Where(x => x.Name == Col.ColumnName).Select(x => x.NewName).FirstOrDefault();
                        ColumnNewOldMap.Add(Col.ColumnName, NewName);

                        NewTable.Columns.Add(NewName);
                    }

                    foreach (DataRow Row in Table.Rows)
                    {
                        var NewRow = NewTable.NewRow();

                        foreach (string OldName in ColumnNewOldMap.Keys)
                        {
                            NewRow[ColumnNewOldMap[OldName]] = Row[OldName];
                        }

                        NewTable.Rows.Add(NewRow);
                    }
                }
            }

            return NewDS;
        }
                
        public Stream GetFileStream()
        {
            return System.IO.File.OpenRead(BaseFilePath + FileName);
        }
    }
}