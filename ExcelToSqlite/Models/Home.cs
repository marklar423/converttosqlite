﻿using System.Collections.Generic;
using ExcelToSqlite.Sources;
using System.Linq;
using System.Data;
using System;
using ExcelToSqlite.Exports.Sqlite;
using System.Web.Mvc;

namespace ExcelToSqlite.Models
{
    public class HomeIndex
    {
        public DataSetProviderOptions FileOptions { get; set; }
    }

    public class ExportOptions
    {
        public string ExportFileName { get; set; }
        public IList<ExportOptionsTable> Tables { get; set; }
        public IEnumerable<SelectListItem> ColumnTypes { get; set; }

        public void Populate()
        {
            var ColTypes = new List<SelectListItem>();
            foreach (var EValue in Enum.GetValues(typeof(SQLiteColumnType)))
            {
                ColTypes.Add(new SelectListItem() { Text = EValue.ToString(), Value = ((int)EValue).ToString() });
            }
            ColumnTypes = ColTypes;

            var SourceFileName = System.IO.Path.GetFileNameWithoutExtension(SessionSource.SourceFileOriginalName);
            ExportFileName = SourceFileName + ".sqlite";
        }

        public void PopulateFrom(DataSet DS)
        {
            Populate();

            Tables = new List<ExportOptionsTable>();

            foreach (DataTable Table in DS.Tables)
            {
                var OptionsTable = new ExportOptionsTable();

                OptionsTable.Name = Table.TableName;
                OptionsTable.NewName = OptionsTable.Name;

                OptionsTable.Columns = new List<ExportOptionsTableColumn>();
                var ColumnsNames = (from DataColumn C in Table.Columns select C.ColumnName).ToList();
                foreach (var ColName in ColumnsNames)
                {
                    var Col = new ExportOptionsTableColumn();
                    Col.Name = ColName;
                    Col.NewName = Col.Name.Replace(" ", "_");
                    OptionsTable.Columns.Add(Col);
                }

                OptionsTable.ShouldIncludeInExport = true;

                Tables.Add(OptionsTable);
            }
        }
    }

    public class ExportOptionsTable
    {
        public string Name { get; set; }
        public string NewName { get; set; }
        public bool ShouldIncludeInExport {get; set;}

        public IList<ExportOptionsTableColumn> Columns { get; set; }
    }

    public class ExportOptionsTableColumn
    {
        public string Name { get; set; }
        public string NewName { get; set; }
        public SQLiteColumnType ColumnType { get; set; }
    }
}