﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExcelToSqlite.Sources
{
    public class SessionSource
    {        
        public static string SourceFileOriginalName
        {
            get
            {
                return HttpContext.Current.Session["file_original_name"] != null ? HttpContext.Current.Session["file_original_name"].ToString() : null;
            }

            set
            {
                HttpContext.Current.Session["file_original_name"] = value;
            }
        }

        public static SourceType? SourceFileType
        {
            get
            {
                return (SourceType) HttpContext.Current.Session["source_type"];
            }

            set
            {
                HttpContext.Current.Session["source_type"] = value;
            }
        }

        public static IDataSetProvider DataSource
        {
            get
            {
                return (IDataSetProvider)HttpContext.Current.Session["data_source"];
            }

            set
            {
                HttpContext.Current.Session["data_source"] = value;
            }
        }

        public static System.Data.DataSet ParseDataSet()
        {
            System.Data.DataSet DS = null;

            if (DataSource != null)
            {
                DS = DataSource.GetData();
            }

            return DS;
        }
    }
}