﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExcelToSqlite.Sources
{
    public enum SourceType
    {
        Excel = 0,
        CSV = 1
    }
}