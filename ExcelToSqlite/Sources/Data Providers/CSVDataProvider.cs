﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.FileIO;

namespace ExcelToSqlite.Sources.Data_Providers
{
    public class CSVDataProvider : IDataSetProvider
    {

        public DataSetProviderOptions Options { get; set; }
        public string SourceFilePath { get; set; }


        public CSVDataProvider(DataSetProviderOptions Options, string SourceFilePath)
        {
            this.Options = Options;
            this.SourceFilePath = SourceFilePath;
        }

        public System.Data.DataSet GetData()
        {
            using (TextFieldParser MyReader = new TextFieldParser(SourceFilePath))
            {
                MyReader.TextFieldType = FieldType.Delimited;
                MyReader.Delimiters = new string[] { "," };
                MyReader.HasFieldsEnclosedInQuotes = true;

                string[] currentRow = null;

                int row = 0;

                DataSet ds = new DataSet();
                DataTable dt = ds.Tables.Add("Table 1");
                

                //Loop through all of the fields in the file. 
                //If any lines are corrupt, report an error and continue parsing. 
                while (!MyReader.EndOfData)
                {
                    try
                    {
                        currentRow = MyReader.ReadFields();
                        
                        //add first column
                        if (row == 0)
                        {
                            if (Options.IsFirstRowAsColumnNames)
                            {
                                foreach (string s in currentRow)
                                {
                                    dt.Columns.Add(s);
                                }
                            }
                            else
                            {
                                for (int i = 0; i < currentRow.Length; i++)
                                {
                                    dt.Columns.Add();
                                }

                                dt.Rows.Add(currentRow);
                            }
                        }
                        else
                        {
                            //add row data
                            dt.Rows.Add(currentRow);
                        }
                    }
                    catch (Microsoft.VisualBasic.FileIO.MalformedLineException ex)
                    {
                        //"Line " + ex.Message + " is invalid.  Skipping";
                    }

                    row++;
                }

                return ds;
            }

        }

        public IEnumerable<System.Data.DataTable> GetTables()
        {
            foreach (DataTable T in GetData().Tables)
            {
                yield return T;
            }            
        }
    }
}