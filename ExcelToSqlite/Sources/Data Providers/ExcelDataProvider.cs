﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Excel;

namespace ExcelToSqlite.Sources.Excel
{
    public class ExcelDataProvider : IDataSetProvider
    {
        public DataSetProviderOptions Options { get; set; }
        public string SourceFilePath { get; set; }


        public ExcelDataProvider(DataSetProviderOptions Options, string SourceFilePath)
        {
            this.Options = Options;
            this.SourceFilePath = SourceFilePath;
        }

        public System.Data.DataSet GetData()
        {
            IExcelDataReader excelReader = null;

            var FilePath = SourceFilePath;

            if (FilePath.EndsWith(".xlsx"))
            {
                excelReader = ExcelReaderFactory.CreateOpenXmlReader(System.IO.File.OpenRead(FilePath));
            }
            else if (FilePath.EndsWith(".xls"))
            {
                excelReader = ExcelReaderFactory.CreateBinaryReader(System.IO.File.OpenRead(FilePath));
            }

            System.Data.DataSet DS = null;

            if (excelReader != null)
            {
                excelReader.IsFirstRowAsColumnNames = Options.IsFirstRowAsColumnNames;
                DS = excelReader.AsDataSet();
                excelReader.Dispose();
            }

            return DS;
        }


        public IEnumerable<DataTable> GetTables()
        {
            foreach (DataTable T in GetData().Tables){
                yield return T;
            }
        }
    }
}