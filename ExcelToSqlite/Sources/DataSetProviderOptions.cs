﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExcelToSqlite.Sources
{
    public class DataSetProviderOptions
    {
        public bool IsFirstRowAsColumnNames { get; set; }
    }
}